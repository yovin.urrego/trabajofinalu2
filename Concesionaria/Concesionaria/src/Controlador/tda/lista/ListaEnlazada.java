/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.tda.lista;

import Controlador.tda.lista.exception.PosicionException;
import Controlador.utiles.TipoOrdenacion;
import Controlador.utiles.Utilidades;
import static Controlador.utiles.Utilidades.getMethod;
import static Controlador.utiles.Utilidades.transformarDatoNumber;
import Modelo.Auto;
import controlador.ordenacion.arreglos.Metodos_1;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author sebastian
 */
//E   T    K   V
//E = T
@XmlRootElement

public class ListaEnlazada<E> {

    private NodoLista<E> cabecera;

    private Integer size;
    private E matriz[];

    @XmlElement
    public NodoLista<E> getCabecera() {
        return cabecera;
    }

    public void setCabecera(NodoLista<E> cabecera) {
        this.cabecera = cabecera;
    }

    /**
     * Constructor de la clase se inicializa la lista en null y el tamanio en 0
     */
    public ListaEnlazada() {
        cabecera = null;
        size = 0;
    }

    /**
     * Permite ver si la lista esta vacia
     *
     * @return Boolean true si esta vacia, false si esta llena
     */
    public Boolean estaVacia() {
        return cabecera == null;
    }

    private void insertar(E dato) {
        NodoLista<E> nuevo = new NodoLista<>(dato, null);
        if (estaVacia()) {
            cabecera = nuevo;
        } else {
            NodoLista<E> aux = cabecera;
            while (aux.getSiguiente() != null) {
                aux = aux.getSiguiente();
            }
            aux.setSiguiente(nuevo);
        }
        size++;
    }

    public void insertarCabecera(E dato) {
        
        if (estaVacia()) {
            insertar(dato);
        } else {
            NodoLista<E> nuevo = new NodoLista<>(dato, null);

            nuevo.setSiguiente(cabecera);
            cabecera = nuevo;
            size++;
        }
    }

    public void insertar(E dato, Integer pos) throws PosicionException {
        //lista size = 1
        if (estaVacia()) {
            insertar(dato);
        } else if (pos >= 0 && pos < size) {
            NodoLista<E> nuevo = new NodoLista<>(dato, null);
            if (pos == (size - 1)) {
                insertar(dato);

            } else {

                NodoLista<E> aux = cabecera;
                for (int i = 0; i < pos - 1; i++) {
                    aux = aux.getSiguiente();
                }
                NodoLista<E> siguiente = aux.getSiguiente();
                aux.setSiguiente(nuevo);
                nuevo.setSiguiente(siguiente);
                size++;
            }

        } else {
            throw new PosicionException("Error en insertar: No existe la posicion dada");
        }
    }

    public  void imprimir() {
        
        NodoLista<E> aux = cabecera;
        
        for (int i = 0; i < getSize(); i++) {
            System.out.print(aux.getDato().toString() + "\t");
            aux = aux.getSiguiente();
          
        }
        
        
        
    }

    public Integer getSize() {
        return size;
    }

    /**
     * Metodo que permite obtener un dato segun la posicion
     *
     * @param pos posicion en la lista
     * @return Elemento
     */
    public E obtenerDato(Integer pos) throws PosicionException {
        if (!estaVacia()) {
            if (pos >= 0 && pos < size) {
                E dato = null;
                if (pos == 0) {
                    dato = cabecera.getDato();
                } else {
                    NodoLista<E> aux = cabecera;
                    for (int i = 0; i < pos; i++) {
                        aux = aux.getSiguiente();
                    }
                    dato = aux.getDato();
                }

                return dato;
            } else {
                throw new PosicionException("Error en obtener dato: No existe la posicion dada");
            }

        } else {
            throw new PosicionException("Error en obtener dato: La lista esta vacia, por endde no hay esa posicion");
        }
    }

    public E eliminarDato(Integer pos) throws PosicionException {
        E auxDato = null;
        if (!estaVacia()) {
            if (pos >= 0 && pos < size) {
                if (pos == 0) {
                    auxDato = cabecera.getDato();
                    cabecera = cabecera.getSiguiente();
                    size--;
                } else {
                    NodoLista<E> aux = cabecera;
                    for (int i = 0; i < pos - 1; i++) {
                        aux = aux.getSiguiente();
                    }
                    auxDato = aux.getDato();
                    NodoLista<E> proximo = aux.getSiguiente();
                    aux.setSiguiente(proximo.getSiguiente());
                    size--;
                }
                return auxDato;

            } else {
                throw new PosicionException("Error en eliminar dato: No existe la posicion dada");
            }

        } else {
            throw new PosicionException("Error en eliminar dato: La lista esta vacia, por endde no hay esa posicion");
        }
    }

    public void vaciar() {
        cabecera = null;
        size = 0;
        //en c utilizamos el free
        //malloc
    }
   
    public void modificarDato(Integer pos, E datoM) throws PosicionException {
        if (!estaVacia()) {
            if (pos >= 0 && pos < size) {
                // E dato = null;
                if (pos == 0) {
                    cabecera.setDato(datoM);
                } else {
                    NodoLista<E> aux = cabecera;
                    for (int i = 0; i < pos; i++) {
                        aux = aux.getSiguiente();
                    }
                    aux.setDato(datoM);
                }

            } else {
                throw new PosicionException("Error en obtener dato: No existe la posicion dada");
            }

        } else {
            throw new PosicionException("Error en obtener dato: La lista esta vacia, por endde no hay esa posicion");
        }
    }

    public E[] toArray() {
        E[] matriz = (E[]) (new Object[this.size]);
        NodoLista<E> aux = cabecera;
        for (int i = 0; i < this.size; i++) {
            matriz[i] = aux.getDato();
//         System.out.print(aux.getDato().toString() + "\t");
            aux = aux.getSiguiente();
        }
        return matriz;
    }

    public ListaEnlazada<E> toList(E[] matriz) {
        //E[] matriz = (E[]) (new Object[this.size]);
        this.vaciar();
        for (E matriz1 : matriz) {
            this.insertar(matriz1);
        }
        return this;
    }

    public ListaEnlazada<E> ordenar_seleccion_burbuja(String atributo, TipoOrdenacion tipoOrdenacion) throws Exception {
        Class<E> clazz = null;
        E[] matriz = null;
        if (size > 0) {
            matriz = toArray();
            E t = null;
            clazz = (Class<E>) cabecera.getDato().getClass();//primitivo, Dato envolvente, Object
            Boolean isObject = Utilidades.isObject(clazz);//si es objeto
            Integer i, j, k = 0;
            Integer n = matriz.length;
            Integer cont = 0;
            Metodos_1 metodos = new Metodos_1();
            for (i = 0; i < n - 1; i++) {
                k = i;
                t = matriz[i];
                for (j = i + 1; j < n; j++) {
                    if (isObject) {
                        Field field = Utilidades.getField(atributo, clazz);
                        Method method = getMethod("get" + Utilidades.capitalizar(atributo), t.getClass());
                        Method method1 = getMethod("get" + Utilidades.capitalizar(atributo), matriz[j].getClass());

                        //llamar a utilidades
                        //if (field.getType().getSuperclass().getSimpleName().equalsIgnoreCase("Number")) {
                        Object[] aux = metodos.evaluaCambiarDato(field.getType(), t, matriz[j], method, method1, tipoOrdenacion, j, matriz);
                        // Object[] aux = evaluaCambiarDato(field.getType(), t, matriz[j], method, method1, tipoOrdenacion, j, matriz);
                        if (aux[0] != null) {
                            t = (E) aux[0];
                            k = (Integer) aux[1];
                            matriz[k] = matriz[i];
                            matriz[i] = t;
                            break;
                        }
                    } else {
                        Object[] aux = metodos.evaluaCambiarDatoNoObjeto(clazz, t, matriz[j], tipoOrdenacion, j, matriz);
                        //Object[] aux = evaluaCambiarDatoNoObjeto(clazz, t, matriz[j], tipoOrdenacion, j, matriz);
                        if (aux[0] != null) {
                            t = (E) aux[0];
                            k = (Integer) aux[1];
                            matriz[k] = matriz[i];
                            matriz[i] = t;
                            break;
                        }
                    }

                }
                //  matriz[k] = matriz[i];//intercambias cuando encountra el valor
                //     matriz[i] = t;
            }
        }
        if (matriz != null) {
            toList(matriz);
        }
        return this;
    }

    public ListaEnlazada<E> ordenar_seleccion_sell(String atributo, TipoOrdenacion tipoOrdenacion) throws Exception {
        Class<E> clazz = null;
        E[] matriz = null;
        Metodos_1 metodos = new Metodos_1();
        if (size > 0) {
            matriz = toArray();
            E t = null;
            clazz = (Class<E>) cabecera.getDato().getClass();//primitivo, Dato envolvente, Object
            Boolean isObject = Utilidades.isObject(clazz);//si es objeto
            Integer n = matriz.length;
            Integer salto = n;
            Integer cont = 0;
            boolean ordenado;
            while (salto > 1) {
                salto = salto / 2;
                do {
                    ordenado = true;
                    for (int j = 0; j <= n - 1 - salto; j++) {
                        t = matriz[j];
                        int k = j + salto;
                        if (isObject) {
                            Field field = Utilidades.getField(atributo, clazz);
                            Method method = getMethod("get" + Utilidades.capitalizar(atributo), t.getClass());
                            Method method1 = getMethod("get" + Utilidades.capitalizar(atributo), matriz[k].getClass());

                            //llamar a utilidades
                            //if (field.getType().getSuperclass().getSimpleName().equalsIgnoreCase("Number")) {
                            Object[] aux = metodos.evaluaCambiarDatoSell(field.getType(), t, matriz[k], method, method1, tipoOrdenacion, k, matriz);
                            if (aux[0] != null) {
                                t = (E) aux[0];
                                k = (Integer) aux[1];
                                matriz[k] = matriz[j];
                                matriz[j] = t;
                                ordenado = (boolean) aux[2];
                            }
                        } else {
                            Object[] aux = metodos.evaluaCambiarDatoNoObjetoSell(clazz, t, matriz[k], tipoOrdenacion, k, matriz);
                            if (aux[0] != null) {
                                t = (E) aux[0];
                                k = (Integer) aux[1];
                                matriz[k] = matriz[j];
                                matriz[j] = t;
                                ordenado = (boolean) aux[2];
                            }

                        }

                    }
                } while (!ordenado);

            }
        }
        if (matriz != null) {
            toList(matriz);
        }
        return this;
    }

    private int division(Class clazz, E[] arreglo, int izquierda, int derecha, String atributo) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        //  System.out.println("pB");
        E pivote = arreglo[izquierda];
        Boolean isObject = Utilidades.isObject(clazz);//si es objeto
        if (isObject) {
            while (true) {
                Field field = Utilidades.getField(atributo, clazz);
                Method method = getMethod("get" + Utilidades.capitalizar(atributo), arreglo[izquierda].getClass());
                Method method1 = getMethod("get" + Utilidades.capitalizar(atributo), arreglo[derecha].getClass());
                Method methodPivo = getMethod("get" + Utilidades.capitalizar(atributo), pivote.getClass());
  
               System.out.println("Gto");

                if (   field.getType().getSuperclass().getSimpleName().equalsIgnoreCase("Number")) {
  //System.out.println("Number");
                    while ( ( (Number) method.invoke(arreglo[izquierda])).doubleValue()<((Number) methodPivo.invoke(pivote)).doubleValue() ) {
                        izquierda++;
                    }
                    while (( (Number) method1.invoke(arreglo[derecha])).doubleValue() > ((Number) methodPivo.invoke(pivote)).doubleValue()) {
                        derecha--;
                    }
                    if (izquierda >= derecha) {

                        return derecha;
                    } else {
                        E temporal = arreglo[izquierda];
                        arreglo[izquierda] = arreglo[derecha];
                        arreglo[derecha] = temporal;
                        izquierda++;
                        derecha--;
                    }

                } else if (Utilidades.isString(   field.getType())) {
                    System.out.println("String");
     
                    while ((String.valueOf(method.invoke(arreglo[izquierda])).toLowerCase().compareTo(String.valueOf(methodPivo.invoke(pivote)).toLowerCase()) < 0)) {
                        izquierda++;
                    }
                    while ((String.valueOf(method1.invoke(arreglo[derecha])).toLowerCase().compareTo(String.valueOf(methodPivo.invoke(pivote)).toLowerCase()) > 0)) {
                        derecha--;
                    }
                    if (izquierda >= derecha) {
                     
                        return derecha;
                    } else {
                        E temporal = arreglo[izquierda];
                        arreglo[izquierda] = arreglo[derecha];
                        arreglo[derecha] = temporal;
                        izquierda++;
                        derecha--;
                    }
                }
            }
        } else {
            while (true) {
                if (clazz.getSuperclass().getSimpleName().equalsIgnoreCase("Number")) {

                    while ((((Number) arreglo[izquierda]).doubleValue() < ((Number) pivote).doubleValue())) {
                        izquierda++;
                    }
                    while ((((Number) arreglo[derecha]).doubleValue() > ((Number) pivote).doubleValue())) {
                        derecha--;
                    }
                    if (izquierda >= derecha) {
                        return derecha;
                    } else {
                        E temporal = arreglo[izquierda];
                        arreglo[izquierda] = arreglo[derecha];
                        arreglo[derecha] = temporal;
                        izquierda++;
                        derecha--;
                    }

                } else if (Utilidades.isString(clazz)) {

                    String datoIzq = (String) arreglo[izquierda];
                    String datoDer = (String) arreglo[derecha];
                    String datoPivo = (String) pivote;
                    while ((datoDer.toLowerCase().compareTo(datoPivo.toLowerCase()) > 0)) {
                        derecha--;
                    }
                    while ((datoIzq.toLowerCase().compareTo(datoPivo.toLowerCase()) < 0)) {
                        izquierda++;
                    }
                    if (izquierda >= derecha) {
                        return derecha;
                    } else {
                        E temporal = arreglo[izquierda];
                        arreglo[izquierda] = arreglo[derecha];
                        arreglo[derecha] = temporal;
                        izquierda++;
                        derecha--;
                    }
                }
            }

        }

    }


    private void QuickShort(String atributo, E[] matriz, int izq, int der, boolean comprobar) {
        System.out.println("dasd");
        Class<E> clazz = null;
        if (izq < der) {
            try {
                clazz = (Class<E>) cabecera.getDato().getClass();//primitivo, Dato envolvente, Object
                int indiceParticion = division(clazz, matriz, izq, der, atributo);
                QuickShort(atributo, matriz, izq, indiceParticion, false);
                QuickShort(atributo, matriz, indiceParticion + 1, der, false);
            } catch (IllegalArgumentException ex) {
                ex.printStackTrace();
            } catch (IllegalAccessException | InvocationTargetException ex) {
                Logger.getLogger(ListaEnlazada.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (comprobar) {
            toList(matriz);
        }
    }

    public void quickShort(String atributo, TipoOrdenacion tipoOrdenacion) {
    E[] matriz = toArray();
                QuickShort(atributo, matriz, 0, matriz.length-1, true);
            rotar(tipoOrdenacion);

    }


    public void rotar(TipoOrdenacion tipoOrdenacion) {
        if (tipoOrdenacion.toString().equalsIgnoreCase(TipoOrdenacion.DESCENDENTE.toString())) {
            E matriz1[] = toArray();
            E[] matriz2 = toArray();
            int size = matriz1.length - 1;

            for (int i = 0, j = matriz1.length - 1; i < matriz1.length; i++, j--) {
                matriz2[i] = matriz1[j];
            }
            toList(matriz2);
        }
    }
    public ListaEnlazada<E> buscar(String atributo, Object valor) throws Exception {
        E[] arreglo = toArray();
        boolean encontrado = false;
        Metodos_1 metodos = new Metodos_1();
        ListaEnlazada listaBusqueda = new ListaEnlazada();
        int inicio = 0;
        int fin = size - 1;
        Class<E> clazz = null;
        clazz = (Class<E>) cabecera.getDato().getClass();//primitivo, Dato envolvente, Object
        Boolean isObject = Utilidades.isObject(clazz);//si es objeto
        while (inicio <= fin && !encontrado) {
            int medio = (inicio + fin) / 2;
            if (isObject) {
                Field field = Utilidades.getField(atributo, clazz);
                Method method = getMethod("get" + Utilidades.capitalizar(atributo), arreglo[medio].getClass());
                if (metodos.isIgualObject(field.getType(), arreglo[medio], valor, method)) { // es igual a valo
                    encontrado = true;
                    if (medio >= 0) {
                        int medioPosMenor = medio;
                        //       System.out.println("de");
                        while (metodos.isIgualObject(field.getType(), arreglo[medioPosMenor], valor, method)) {
                            listaBusqueda.insertar(arreglo[medioPosMenor]);
                            if (medioPosMenor > 0) {
                                medioPosMenor--;
                            } else {
                                break;
                            }
                        }
                    }
                    if (medio <= size - 1) {
                        if (medio == size - 1) {
                            //    int medioPosMayor = medio + 1;
                        } else {
                            int medioPosMayor = medio + 1;
                            while (metodos.isIgualObject(field.getType(), arreglo[medioPosMayor], valor, method)) {
                                listaBusqueda.insertar(arreglo[medioPosMayor]);
                                if (medioPosMayor < size - 1) {
                                    medioPosMayor++;
                                } else {
                                    break;
                                }
                            }
                        }
                    }
                } else {
                    if (metodos.isMayorObject(field.getType(), arreglo[medio], valor, method)) {
                        fin = medio - 1;
                    } else {
                        inicio = medio + 1;
                    }
                }
            } else {

                if (metodos.isIgualNoObject(clazz, arreglo[medio], valor)) { // es igual a valo
                    encontrado = true;
                    listaBusqueda.insertar(arreglo[medio]);
                    if (medio >= 0) {
                        int medioPosMenor = medio - 1;
                        while (metodos.isIgualNoObject(clazz, arreglo[medioPosMenor], valor)) {
                            listaBusqueda.insertar(arreglo[medioPosMenor]);
                            medioPosMenor--;
                        }
                    }
                    if (medio <= size - 1) {
                        if (medio == size - 1) {
                            //    int medioPosMayor = medio + 1;
                        } else {
                            int medioPosMayor = medio + 1;
                            while (metodos.isIgualNoObject(clazz, arreglo[medioPosMayor], valor)) {
                                listaBusqueda.insertar(arreglo[medioPosMayor]);
                                medioPosMayor++;
                            }
                        }
                    }
                } else {
                    if (metodos.isMayorNoObject(clazz, arreglo[medio], valor)) {
                        fin = medio - 1;
                    } else {
                        inicio = medio + 1;
                    }
                }
            }
        }
        listaBusqueda.insertarCabecera(encontrado);
        return listaBusqueda;
    }

}
