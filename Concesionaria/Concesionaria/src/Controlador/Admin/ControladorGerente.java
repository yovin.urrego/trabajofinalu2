/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador.Admin;

import Controlador.tda.lista.ListaEnlazadaServices;
import Controlador.tda.lista.exception.PosicionException;
import Modelo.Auto;
import Modelo.Factura;
import VIsta.PrincipalAdmin;
import com.google.gson.Gson;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author User
 */
public class ControladorGerente {
    PrincipalAdmin m = new PrincipalAdmin();
    ListaEnlazadaServices<Factura> listafac = new ListaEnlazadaServices();
    ListaEnlazadaServices<Auto> lista = new ListaEnlazadaServices();
    
     public void cargar() throws IOException {
        Gson json = new Gson();
        FileReader file = new FileReader("datos" + File.separatorChar + "Autos.json");
        String V = "";
        int valor = file.read();
        StringBuilder st = new StringBuilder();
        while (valor != -1) {
            st.append(String.valueOf((char) valor));
            valor = file.read();

        }
        V = st.toString();
        Auto[] Vehiculo = json.fromJson(V, Auto[].class);

        for (int i = Vehiculo.length - 1; i >= 0; i--) {
            lista.insertarAlInicio(Vehiculo[i]);
        }

    }

    public void cargarFacturas() throws IOException {
        Gson json = new Gson();
        FileReader file = new FileReader("datos" + File.separatorChar + "Facturas.json");
        String V = "";
        int valor = file.read();
        StringBuilder st = new StringBuilder();
        while (valor != -1) {
            st.append(String.valueOf((char) valor));
            valor = file.read();

        }
        V = st.toString();
        Factura[] fac = json.fromJson(V, Factura[].class);

        for (int i = fac.length - 1; i >= 0; i--) {
            listafac.insertarAlInicio(fac[i]);
        }

    }

//    private void Eliminar(int m) {
//
//        String valor = jTable2.getValueAt(m, 0).toString();
//        try {
//            if (m >= 0) {
//                JOptionPane.showMessageDialog(null, "Auto" + " " + valor + " " + "Vendido");
//                lista.eliminarPosicion(m);
//                actualizarcarros();
//                jTable2.setModel((this.cargarTabla()));
//            } else {
//                JOptionPane.showMessageDialog(null, "Ocurrio un erro");
//            }
//        } catch (Exception e) {
//            JOptionPane.showMessageDialog(null, "Ocurrio un erro");
//        }
//
//    }

    public DefaultTableModel cargarTabla() {
        DefaultTableModel modelo = new DefaultTableModel();
        modelo.addColumn("Marca");
        modelo.addColumn("Placa");
        modelo.addColumn("Precio");
        modelo.addColumn("Color");
        modelo.addColumn("Matricula");

        String datos[] = new String[5];
        for (int i = 0; i < lista.getSize(); i++) {
            datos[0] = String.valueOf(lista.obtenerDato(i).getMarca());
            datos[1] = lista.obtenerDato(i).getPlaca();
            datos[2] = String.valueOf(lista.obtenerDato(i).getCosto());
            datos[3] = String.valueOf(lista.obtenerDato(i).getColor());
            datos[4] = String.valueOf(lista.obtenerDato(i).getMatricula());
            modelo.addRow(datos);

        }

        return modelo;
    }
    
    public void guardar() throws PosicionException, IOException {

        Gson json = new Gson();
        Factura[] fac = new Factura[listafac.getSize()];

        for (int i = 0; i < fac.length; i++) {
            fac[i] = listafac.obtenerDato(i);
        }
        String jsonString = json.toJson(fac);
        FileWriter file = new FileWriter("datos" + File.separatorChar + "Facturas.json");
        file.write(jsonString);
        file.flush();
    }

    public void actualizarcarros() throws PosicionException, IOException {
        Gson json = new Gson();
        Auto[] arrayP = new Auto[lista.getSize()];

        for (int i = 0; i < arrayP.length; i++) {
            arrayP[i] = lista.obtenerDato(i);
        }
        String jsonString = json.toJson(arrayP);
        FileWriter file = new FileWriter("datos" + File.separatorChar + "Autos.json");
        file.write(jsonString);
        file.flush();
    }
}
