/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador.Factura;

import Modelo.Auto;
import Modelo.Cliente;
import Modelo.Factura;

/**
 *
 * @author Yovin
 */
public class CtrlFactura extends Factura{

    public CtrlFactura(Integer numero, Cliente cliente, Auto auto) {
        super(numero, cliente, auto);
    }
    
}
