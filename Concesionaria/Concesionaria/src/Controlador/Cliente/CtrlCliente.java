/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador.Cliente;

import Modelo.Cliente;

/**
 *
 * @author Yovin
 */
public class CtrlCliente extends Cliente {

    public CtrlCliente(String nombre, String apellido, String cedula) {
        super(nombre, apellido, cedula);
    }

    public CtrlCliente() {
    }
    
}
