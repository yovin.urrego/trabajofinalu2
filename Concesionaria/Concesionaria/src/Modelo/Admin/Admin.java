/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modelo.Admin;

/**
 *
 * @author Jordy
 */
public class Admin {
    private String rol ;
    private String nombre ;
     private String apellido;
    private String clave;

    public Admin(String nombre, String apellido, String clave,boolean isAdmin) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.clave = clave;
        if (isAdmin) {
            rol = "admin";
        }else{
            rol="gerente";
        }
    }

    public String getRol() {
        return rol;
    }

   
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }
   

}
