    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modelo;

/**
 *
 * @author Jordy
 */
public class Auto {
    private String marca;
    private String placa;
    private Integer costo;
   private String color;
  private String matricula;

    @Override
    public String toString() {
        return "Auto{" + "marca=" + marca + ", placa=" + placa + ", costo=" + costo + ", color=" + color + ", matricula=" + matricula + '}';
    }
  
    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }
  
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
   public Auto(){
       
   }
   
    public Auto(String marca, String placa, Integer costo,String color,String matricula) {
        this.marca = marca;
        this.placa = placa;
        this.costo = costo;
        this.color = color;
        this.matricula=matricula;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public Integer getCosto() {
        return costo;
    }

    public void setCosto(Integer costo) {
        this.costo = costo;
    }
   
 
    
}
