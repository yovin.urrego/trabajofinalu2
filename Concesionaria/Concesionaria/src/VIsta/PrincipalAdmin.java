/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VIsta;

import Modelo.Auto;
import com.google.gson.Gson;
import Controlador.tda.lista.*;
import Controlador.tda.lista.exception.PosicionException;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Jordy
 */
public class PrincipalAdmin extends javax.swing.JFrame {

    ListaEnlazadaServices<Auto> lista = new ListaEnlazadaServices();
    Integer fila = -1;

    /**
     * Creates new form PrincipalAdmin
     */
    public PrincipalAdmin() {
        this.initComponents();
        this.setLocationRelativeTo(null);
        try {
            cargar();

        } catch (IOException ex) {
            Logger.getLogger(PrincipalAdmin.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.jTable1.setModel(cargarTabla());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        matricula = new javax.swing.JTextField();
        color = new javax.swing.JTextField();
        precio = new javax.swing.JTextField();
        placa = new javax.swing.JTextField();
        marca = new javax.swing.JTextField();
        agregar = new javax.swing.JButton();
        Eliminar = new javax.swing.JButton();
        Actualizar = new javax.swing.JButton();
        atras = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel7 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel3.setLayout(null);

        jPanel2.setBackground(new java.awt.Color(204, 204, 204));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createMatteBorder(5, 5, 5, 5, new java.awt.Color(153, 0, 0)), "Vender Auto", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N
        jPanel2.setLayout(null);

        jLabel1.setFont(new java.awt.Font("Yu Gothic Medium", 1, 14)); // NOI18N
        jLabel1.setText("Marca:");
        jPanel2.add(jLabel1);
        jLabel1.setBounds(70, 30, 60, 30);

        jLabel2.setFont(new java.awt.Font("Yu Gothic Medium", 1, 14)); // NOI18N
        jLabel2.setText("Placa:");
        jPanel2.add(jLabel2);
        jLabel2.setBounds(70, 60, 60, 30);

        jLabel3.setFont(new java.awt.Font("Yu Gothic Medium", 1, 14)); // NOI18N
        jLabel3.setText("Precio:");
        jPanel2.add(jLabel3);
        jLabel3.setBounds(70, 90, 60, 30);

        jLabel4.setFont(new java.awt.Font("Yu Gothic Medium", 1, 14)); // NOI18N
        jLabel4.setText("Color:");
        jPanel2.add(jLabel4);
        jLabel4.setBounds(70, 120, 60, 30);

        jLabel5.setFont(new java.awt.Font("Yu Gothic Medium", 1, 14)); // NOI18N
        jLabel5.setText("Matricula:");
        jPanel2.add(jLabel5);
        jLabel5.setBounds(70, 150, 80, 30);
        jPanel2.add(matricula);
        matricula.setBounds(160, 150, 120, 30);
        jPanel2.add(color);
        color.setBounds(160, 120, 120, 30);
        jPanel2.add(precio);
        precio.setBounds(160, 90, 120, 30);
        jPanel2.add(placa);
        placa.setBounds(160, 60, 121, 30);

        marca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                marcaActionPerformed(evt);
            }
        });
        jPanel2.add(marca);
        marca.setBounds(160, 30, 120, 30);

        agregar.setBackground(new java.awt.Color(51, 51, 51));
        agregar.setFont(new java.awt.Font("Yu Gothic Medium", 1, 14)); // NOI18N
        agregar.setForeground(new java.awt.Color(255, 255, 255));
        agregar.setText("Agregar");
        agregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                agregarActionPerformed(evt);
            }
        });
        jPanel2.add(agregar);
        agregar.setBounds(330, 30, 110, 30);

        Eliminar.setBackground(new java.awt.Color(51, 51, 51));
        Eliminar.setFont(new java.awt.Font("Yu Gothic Medium", 1, 14)); // NOI18N
        Eliminar.setForeground(new java.awt.Color(255, 255, 255));
        Eliminar.setText("Eliminar");
        Eliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EliminarActionPerformed(evt);
            }
        });
        jPanel2.add(Eliminar);
        Eliminar.setBounds(330, 70, 110, 30);

        Actualizar.setBackground(new java.awt.Color(51, 51, 51));
        Actualizar.setFont(new java.awt.Font("Yu Gothic Medium", 1, 14)); // NOI18N
        Actualizar.setForeground(new java.awt.Color(255, 255, 255));
        Actualizar.setText("Actualizar");
        Actualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ActualizarActionPerformed(evt);
            }
        });
        jPanel2.add(Actualizar);
        Actualizar.setBounds(330, 110, 110, 30);

        atras.setBackground(new java.awt.Color(51, 51, 51));
        atras.setFont(new java.awt.Font("Yu Gothic Medium", 1, 14)); // NOI18N
        atras.setForeground(new java.awt.Color(255, 255, 255));
        atras.setText("Atras");
        atras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                atrasActionPerformed(evt);
            }
        });
        jPanel2.add(atras);
        atras.setBounds(330, 150, 110, 30);

        jPanel3.add(jPanel2);
        jPanel2.setBounds(80, 110, 515, 200);

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createMatteBorder(5, 5, 5, 5, new java.awt.Color(153, 0, 0)), "Lista Autos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 477, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(27, Short.MAX_VALUE))
        );

        jPanel3.add(jPanel1);
        jPanel1.setBounds(80, 330, 515, 200);

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/VIsta/Imagenes/Logo.jpg"))); // NOI18N
        jLabel7.setBorder(javax.swing.BorderFactory.createMatteBorder(5, 5, 5, 5, new java.awt.Color(153, 0, 0)));
        jPanel3.add(jLabel7);
        jLabel7.setBounds(200, 20, 240, 70);

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/VIsta/Imagenes/Fondo2.jpg"))); // NOI18N
        jPanel3.add(jLabel6);
        jLabel6.setBounds(0, 0, 670, 580);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 670, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 586, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
public void cargar() throws IOException {
        Gson json = new Gson();
        FileReader file = new FileReader("datos" + File.separatorChar + "Autos.json");
        String auxiliar = "";
        int valor = file.read();
        StringBuilder st = new StringBuilder();
        while (valor != -1) {
            st.append(String.valueOf((char) valor));
            valor = file.read();

        }
        auxiliar = st.toString();
        Auto[] autos = json.fromJson(auxiliar, Auto[].class);

        for (int i = autos.length - 1; i >= 0; i--) {
            lista.insertarAlInicio(autos[i]);
        }

    }
    private void marcaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_marcaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_marcaActionPerformed
    public DefaultTableModel cargarTabla() {
        DefaultTableModel modelo = new DefaultTableModel();
        modelo.addColumn("Marca");
        modelo.addColumn("Placa");
        modelo.addColumn("Precio");
        modelo.addColumn("Color");
        modelo.addColumn("Matricula");

        String datos[] = new String[5];
        for (int i = 0; i < lista.getSize(); i++) {
            datos[0] = String.valueOf(lista.obtenerDato(i).getMarca());
            datos[1] = lista.obtenerDato(i).getPlaca();
            datos[2] = String.valueOf(lista.obtenerDato(i).getCosto());
            datos[3] = String.valueOf(lista.obtenerDato(i).getColor());
            datos[4] = String.valueOf(lista.obtenerDato(i).getMatricula());
            modelo.addRow(datos);

        }

        return modelo;
    }

    public void guardar() throws PosicionException, IOException {

        Gson json = new Gson();

        Auto[] arrayP = new Auto[lista.getSize()];

        for (int i = 0; i < arrayP.length; i++) {
            arrayP[i] = lista.obtenerDato(i);
        }
        String jsonString = json.toJson(arrayP);
        FileWriter file = new FileWriter("datos" + File.separatorChar + "Autos.json");
        file.write(jsonString);
        file.flush();
    }

    public void limpiar() {
        this.marca.setText(null);
        this.placa.setText(null);
        this.precio.setText(null);
        this.color.setText(null);
        this.matricula.setText(null);
    }
    private void agregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_agregarActionPerformed
        if (marca.getText().trim().isEmpty() || matricula.getText().isEmpty() || placa.getText().isEmpty() || precio.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Campos Vacíos");

        } else {
            try {
                lista.insertarAlInicio(new Auto(marca.getText(), placa.getText(), Integer.parseInt(precio.getText()), color.getText(), matricula.getText()));
                guardar();
                this.jTable1.setModel(cargarTabla());
                System.out.println("creado");
            } catch (Exception e) {
                System.out.println("error" + e);
            }
        }
        limpiar();
    }//GEN-LAST:event_agregarActionPerformed

    private void EliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EliminarActionPerformed

        fila = jTable1.getSelectedRow();
        //String valor = jTable1.getValueAt(fila, 0).toString();
        //JOptionPane.showMessageDialog(null, "seleccione una fila");
        if (fila >= 0) {
            try {
                if (fila >= 0) {
                    lista.eliminarPosicion(fila);
                    guardar();
                    JOptionPane.showMessageDialog(null, "Se elimino correctamente el auto\t");
                    jTable1.setModel((this.cargarTabla()));
                } else {
                    JOptionPane.showMessageDialog(null, "Ocurrio un error");
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Ocurrio un error");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Porfavor seleccione un auto");
        }
        limpiar();

    }//GEN-LAST:event_EliminarActionPerformed

    private void ActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ActualizarActionPerformed
        fila = jTable1.getSelectedRow();
        //String valor = jTable1.getValueAt(fila, 0).toString();
        if (fila >= 0) {
            try {
                Auto a = new Auto(marca.getText(), placa.getText(), Integer.parseInt(precio.getText()), color.getText(), matricula.getText());
                lista.modificarDatoPosicion(fila, a);
                JOptionPane.showMessageDialog(null, "Se actualizo correctamente el auto\t");
                jTable1.setModel((this.cargarTabla()));
                guardar();

            } catch (PosicionException ex) {
                Logger.getLogger(PrincipalAdmin.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(PrincipalAdmin.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Porfavor seleccione un auto");
        }
        limpiar();
    }//GEN-LAST:event_ActualizarActionPerformed

    private void atrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_atrasActionPerformed
        Login l = new Login();
        l.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_atrasActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        // TODO add your handling code here:
        fila = jTable1.getSelectedRow();
        if (fila >= 0) {
            try {
                marca.setText(lista.getLista().obtenerDato(fila).getMarca());
                placa.setText(lista.getLista().obtenerDato(fila).getPlaca());
                precio.setText(lista.getLista().obtenerDato(fila).getCosto().toString());
                color.setText(lista.getLista().obtenerDato(fila).getColor());
                matricula.setText(lista.getLista().obtenerDato(fila).getMatricula());
            } catch (PosicionException ex) {
                Logger.getLogger(PrincipalAdmin.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_jTable1MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PrincipalAdmin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PrincipalAdmin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PrincipalAdmin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PrincipalAdmin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {

                new PrincipalAdmin().setVisible(true);

            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Actualizar;
    private javax.swing.JButton Eliminar;
    private javax.swing.JButton agregar;
    private javax.swing.JButton atras;
    private javax.swing.JTextField color;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField marca;
    private javax.swing.JTextField matricula;
    private javax.swing.JTextField placa;
    private javax.swing.JTextField precio;
    // End of variables declaration//GEN-END:variables
}
