/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.contrasenas;

import controlador.tda.lista.ListaEnlazada;
import java.util.Scanner;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelo.admin.Admin;
import org.springframework.security.crypto.bcrypt.BCrypt;

/**
 *
 * @author Yovin
 */
public class verificarPassword {

    // Creating a private instance
    // of Scanner class
    //   private static Scanner sc;
    // BCrypt is a password Hashing
    // Function based on Blowfish
    // Algorithm.
    public String Password_Hash(String password) {
        return BCrypt.hashpw( password, BCrypt.gensalt());
    }

    // Verifying password with the
    // hashed password.
    public boolean Verify_Password(ListaEnlazada adminL, String password, String hashed_password) {
        try {
            if ((boolean) adminL.obtenerDato(0)) {
                for (int i = 1; i < adminL.getSize(); i++) {
                    if (BCrypt.checkpw(password, ((Admin) adminL.obtenerDato(i)).getPassword())) {
                        return true;
                    } 
                }
            } else {
                return false;
            }
        } catch (Exception ex) {
            return false;
        }
        return false;
    }

//    public static void main(
//            String args[]) throws Exception {
//
//        // Scanner class instance connected
//        // to the Input Stream(System.in)
//        sc = new Scanner(System.in);
//
//        System.out.println( "Enter the password: ");
//
//        // Scanner class instance
//        // reading the user input
//        String p = sc.nextLine();
//
//        // Generate hashed password
//        String passwordHash = Password_Hash(p);
//
//        // Print Hashed Password
//        System.out.println(   "Hashed-password: " + passwordHash);
//
//        // Printing the result of verification
//        // of hashed password
//        // with original password
//        System.out.println( "Verification: " + Verify_Password(  p, passwordHash));
//    }
}
