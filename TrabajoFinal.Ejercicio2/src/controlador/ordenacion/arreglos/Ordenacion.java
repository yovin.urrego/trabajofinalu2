/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.ordenacion.arreglos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/**
 *
 * @author sebastian
 */
public class Ordenacion {

    private Integer matriz[];
    private static Integer SIZE = 500000;

    public Integer[] getMatriz() {
        return matriz;
    }

    public void llenarArchivo() {
        try {
            FileWriter file = new FileWriter("datos" + File.separatorChar + "numero.txt");
            for (int i = 0; i < SIZE; i++) {
                Integer aux = (int) (Math.random() * 1000);
                file.write(aux.toString() + "\n");
            }
            file.close();
        } catch (Exception e) {
            System.out.println("Error " + e);
        }

    }

    public void llenarMatriz() {
        try {
            matriz = new Integer[20];
            FileReader fr = new FileReader("datos" + File.separatorChar + "numero.txt");
            BufferedReader entrada = new BufferedReader(fr);
            String aux = entrada.readLine();
            Integer cont = 0;
            while (aux != null) {
                matriz[cont] = Integer.parseInt(aux);
                aux = entrada.readLine();
                cont++;
            }
            fr.close();
            entrada.close();
        } catch (Exception e) {
        }

    }

    public void imprimir() {
        System.out.println("***********************");
        for (int i = 0; i < matriz.length; i++) {
            System.out.print(matriz[i] + "\t");
        }
        System.out.println("");
        System.out.println("***********************");
    }

    public void burbuja() {
        Integer cont = 0;
//        for (int i = 0; i < matriz.length; i++) {
//            for (int j = 0; j < matriz.length - 1; j++) {
//                if (matriz[j] > matriz[j + 1]) {
//                    Integer aux = matriz[j];
//                    matriz[j] = matriz[j + 1];
//                    matriz[j + 1] = aux;
//                    cont++;
//                }
//
//            }
//        }
        int matriz[] = {1, 4, 2, 5, 6, 7};

        for (int i = matriz.length; i > 1; i--) {
            Integer aux = 0;
            Integer t = 0;
            for (int j = 0; j < i - 1; j++) {
                if (matriz[j] > matriz[j + 1]) {
                    aux = matriz[j];
                    t = j;
                }

            }
            matriz[t] = matriz[t + 1];
            matriz[t + 1] = aux;
        }
           for (int l = 0; l < 6; l++) {
            System.out.println(matriz[l]);
        }
        System.out.println("INTERCAMBIOS " + cont);
    }

    public void shakeSort(Integer[] matriz) {
        Integer cont = 0;
        Integer i, j, izq, der;
        izq = 1;
        der = matriz.length - 1;
        j = matriz.length - 1;
        do {

            for (i = der; i >= izq; i--) {
                if (matriz[i - 1] > matriz[i]) {
                    Integer aux = matriz[i];
                    matriz[i] = matriz[i - 1];
                    matriz[i - 1] = aux;
                    j = i;
                    cont++;
                }

            }
            izq = j + 1;
            for (i = izq; i <= der; i++) {
                if (matriz[i - 1] > matriz[i]) {
                    Integer aux = matriz[i];
                    matriz[i] = matriz[i - 1];
                    matriz[i - 1] = aux;
                    j = i;
                    cont++;
                }

            }
            der = j - 1;
        } while (izq <= der);

        System.out.println("INTERCAMBIOS " + cont);
    }

    public void insercion(Integer[] matriz) {
        Integer cont = 0;
        int j, aux = 0;
        for (int i = 1; i < matriz.length; i++) {
            j = i - 1;
            aux = matriz[i]; // 4   7   9   1  2
            while (j >= 0 && aux < matriz[j]) {
                matriz[j + 1] = matriz[j];
                j = j - 1;
                cont++;
            }
            matriz[j + 1] = aux;
        }
        System.out.println("INTERCAMBIOS " + cont);
//        for (i = 1; i < n; i++) {
//            j = i - 1;
//            t = a[i];
//            while (j >= 0 && t < a[j]) {
//                a[j + 1] = a[j];
//                j = j - 1;
//            }
//            a[j + 1] = t;
//        }

    }

    public void seleccion(Integer[] matriz) {
        Integer i, j, k, t = 0;
        Integer n = matriz.length;
        Integer cont = 0;
        for (i = 0; i < n - 1; i++) {
            k = i;
            t = matriz[i];
            for (j = i + 1; j < n; j++) {
                if (matriz[j] < t) {
                    t = matriz[j];//
                    k = j;//intercambio
                    cont++;
                }
            }
            matriz[k] = matriz[i];//intercambias cuando encountra el valor
            matriz[i] = t;
            cont++;
        }
//        for(i=0;i<n-1;i++) {
//   k=i;
//  t=a[i];
//  for (j=i+1; j<n; j++) {
//   	if (a[j] < t) {
//      	     t= a[j];
//      	     k=j;
//    	}
//  }
// a[k]= a[i];
// a[i]= t;
//}
     
        System.out.println("INTERCAMBIOS " + cont);
    }

    public static void quicksort(Integer A[], int izq, int der) {

        int pivote = A[izq]; // tomamos primer elemento como pivote
        int i = izq;         // i realiza la búsqueda de izquierda a derecha
        int j = der;         // j realiza la búsqueda de derecha a izquierda
        int aux;

        while (i < j) {                          // mientras no se crucen las búsquedas                                   
            while (A[i] <= pivote && i < j) {
                i++; // busca elemento mayor que pivote
            }
            while (A[j] > pivote) {
                j--;           // busca elemento menor que pivote
            }
            if (i < j) {                        // si no se han cruzado                      
                aux = A[i];                      // los intercambia
                A[i] = A[j];
                A[j] = aux;
            }
        }

        A[izq] = A[j];      // se coloca el pivote en su lugar de forma que tendremos                                    
        A[j] = pivote;      // los menores a su izquierda y los mayores a su derecha

        if (izq < j - 1) {
            quicksort(A, izq, j - 1);          // ordenamos subarray izquierdo
        }
        if (j + 1 < der) {
            quicksort(A, j + 1, der);          // ordenamos subarray derecho
        }
    }

    public static void main(String[] args) {
        //LLAMADA METODO BURBUJA
        //ITERACIONES 99751457
        //Tiempo 2.0  seg   2:99

        //LLAMADA METODO BURBUJA MEJORADA
        //ITERACIONES 99751457    99751457  3 seg
        //Tiempo 3.0  3 seg
        //LLAMADA METODO INSERCCION
        //ITERACIONES 99751457
        //Tiempo 2.0  seg
        //LLAMADA METODO SELECCION
        //INTERCAMBIOS 127842
        //Tiempo 2.0   2:04
        Ordenacion o = new Ordenacion();

//o.llenarArchivo();
        o.llenarMatriz();
        //  o.imprimir();
        long inicio = System.currentTimeMillis();
        System.out.println("LLAMADA METODO Qsort");
//      Integer 
        //Integer pos = o.getMatriz()/2;
        System.out.println(o.getMatriz().length);
        o.quicksort(o.getMatriz(), 0, ((int)(o.getMatriz().length/2)));

        long fin = System.currentTimeMillis();
        double tiempo = (double) ((fin - inicio) / 1000);
        System.out.println("Tiempo " + tiempo);
        o.imprimir();
    }
}
