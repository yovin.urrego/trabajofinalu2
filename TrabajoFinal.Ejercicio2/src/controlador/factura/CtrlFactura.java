/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.factura;

import modelo.factura.Factura;

/**
 *
 * @author Yovin
 */
public class CtrlFactura extends Factura{

    public CtrlFactura(String fechaEmitida, String cliente, Double totalSinIva, String inmueble) {
        super(fechaEmitida, cliente, totalSinIva, inmueble);
    }
    
}
