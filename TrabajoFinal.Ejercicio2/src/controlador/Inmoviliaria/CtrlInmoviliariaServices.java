/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.Inmoviliaria;

import com.google.gson.Gson;
import controlador.tda.lista.ListaEnlazadaServices;
import controlador.tda.lista.exception.PosicionException;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import modelo.binmuebles.BienInmueble;
import modelo.cliente.Cliente;
import modelo.factura.Factura;

/**
 *
 * @author Yovin
 */
public class CtrlInmoviliariaServices {
    
    private CtrlInmoviliaria inmoviliaria = new CtrlInmoviliaria();
    
    public CtrlInmoviliaria getInmoviliaria() {
        return this.inmoviliaria;
    }
    
    public void setClientes(ListaEnlazadaServices<Cliente> clientes) {
        this.inmoviliaria.setClientes(clientes);
    }
    
    public void modificarInmueble(Integer posicion, BienInmueble inmueble) {
        inmoviliaria.modificarInmueble(posicion, inmueble);
    }
    
    public void eliminarInmueble(Integer posicion) {
        inmoviliaria.eliminarInmueble(posicion);
    }
    
    public void agregarInmueble(BienInmueble inmueble) {
        inmoviliaria.agregarInmueble(inmueble);
    }
    
    public void agregarFactura(BienInmueble inmueble, Cliente cliente) {
        
        inmoviliaria.agregarFactura(inmueble, cliente);
    }
    
    public void cargarInmuebles() throws IOException {
        inmoviliaria.cargarInmuebles();
    }
    
    public void cargarClientes() throws IOException {
        inmoviliaria.cargarClientes();
    }
    
    public void guardarInmuebles() throws PosicionException, IOException {
        inmoviliaria.guardarInmuebles();
    }
    
    public void guardarFacturas() throws PosicionException, IOException {
        inmoviliaria.guardarFacturas();
    }
    
    public void guardarClientes() throws PosicionException, IOException {
        inmoviliaria.guardarClientes();
    }
    
}
