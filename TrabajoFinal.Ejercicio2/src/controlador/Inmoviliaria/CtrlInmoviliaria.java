/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.Inmoviliaria;

import com.google.gson.Gson;
import controlador.tda.lista.ListaEnlazadaServices;
import controlador.tda.lista.exception.PosicionException;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import modelo.binmuebles.BienInmueble;
import modelo.binmuebles.Inmobiliaria;
import modelo.cliente.Cliente;
import modelo.factura.Factura;

/**
 *
 * @author Yovin
 */
public class CtrlInmoviliaria extends Inmobiliaria {
    
    public void modificarInmueble(Integer posicion, BienInmueble inmueble) {
        super.setInmueble(inmueble, posicion);        
    }

    public void eliminarInmueble(Integer posicion) {
        super.deleteInmueble(posicion);
    }
    
    public void agregarInmueble(BienInmueble inmueble) {
        super.addInmueble(inmueble);
    }
    
        public void agregarCliente(Cliente cliente) {
        super.addClientes(cliente);
    }
    
    public void agregarFactura(BienInmueble inmueble, Cliente cliente) {
        
        LocalDate todaysDate = LocalDate.now();
        Factura factura = new Factura(todaysDate.toString(), (cliente.getNombre()), inmueble.getCosto(), inmueble.getUbicacion());
        super.addFactura(factura);
        super.addClientes(cliente);
    }

      public void setClientes(ListaEnlazadaServices<Cliente> clientes) {
      super.setClientes(clientes);
    }
    
    public void cargarInmuebles() throws IOException {
        Gson json = new Gson();
        FileReader file = new FileReader("datos" + File.separatorChar + "inmuebles" + File.separatorChar + "inmuebles.json");
        String auxiliar = "";
        int valor = file.read();
        StringBuilder st = new StringBuilder();
        while (valor != -1) {
            st.append(String.valueOf((char) valor));
            valor = file.read();
        }
        auxiliar = st.toString();
        BienInmueble[] inmueble = json.fromJson(auxiliar, BienInmueble[].class);
        for (int i = inmueble.length - 1; i >= 0; i--) {
            super.addInmueble(inmueble[i]);
        }        
    }
    
       public void cargarClientes() throws IOException {
        Gson json = new Gson();
        FileReader file = new FileReader("datos" + File.separatorChar + "clientes" + File.separatorChar + "clientes.json");
        String auxiliar = "";
        int valor = file.read();
        StringBuilder st = new StringBuilder();
        while (valor != -1) {
            st.append(String.valueOf((char) valor));
            valor = file.read();
        }
        auxiliar = st.toString();
        Cliente[] clientes = json.fromJson(auxiliar, Cliente[].class);
        for (int i = clientes.length - 1; i >= 0; i--) {
            super.addClientes(clientes[i]);
        }        
    }
    
    public void guardarInmuebles() throws PosicionException, IOException {
        Gson json = new Gson();        
        BienInmueble[] arrayP = new BienInmueble[super.getListaInmuebles().getSize()];
        for (int i = 0; i < arrayP.length; i++) {
            arrayP[i] = super.getListaInmuebles().obtenerDato(i);
        }
        String jsonString = json.toJson(arrayP);
        FileWriter file = new FileWriter("datos" + File.separatorChar + "inmuebles" + File.separatorChar + "inmuebles.json");
        file.write(jsonString);
        file.flush();
    }    
    
    public void guardarFacturas() throws PosicionException, IOException {
        Gson json = new Gson();        
        Factura[] arrayP = new Factura[super.getFasturas().getSize()];
        for (int i = 0; i < arrayP.length; i++) {
            arrayP[i] = super.getFasturas().obtenerDato(i);
        }
        String jsonString = json.toJson(arrayP);
        FileWriter file = new FileWriter("datos" + File.separatorChar + "facturas" + File.separatorChar + "facturas.json");
        file.write(jsonString);
        file.flush();
    }    
    
        public void guardarClientes() throws PosicionException, IOException {
        Gson json = new Gson();        
        Cliente[] arrayP = new Cliente[super.getClientes().getSize()];
        for (int i = 0; i < arrayP.length; i++) {
            arrayP[i] = super.getClientes().obtenerDato(i);
        }
        String jsonString = json.toJson(arrayP);
        FileWriter file = new FileWriter("datos" + File.separatorChar + "clientes" + File.separatorChar + "clientes.json");
        file.write(jsonString);
        file.flush();
    }    
}
