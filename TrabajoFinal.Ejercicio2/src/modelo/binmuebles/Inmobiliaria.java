/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo.binmuebles;

import com.google.gson.Gson;
import controlador.tda.lista.ListaEnlazadaServices;
import controlador.tda.lista.exception.PosicionException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import modelo.cliente.Cliente;
import modelo.factura.Factura;

/**
 *
 * @author Yovin
 */
public class Inmobiliaria {

    private ListaEnlazadaServices<BienInmueble> listaInmuebles = new ListaEnlazadaServices<>();
    private ListaEnlazadaServices<Factura> fasturas = new ListaEnlazadaServices<>();
    private ListaEnlazadaServices<Cliente> clientes = new ListaEnlazadaServices<>();

    /**
     * @return the listaInmuebles
     */
    public ListaEnlazadaServices<BienInmueble> getListaInmuebles() {
        return listaInmuebles;
    }

    public void addInmueble(BienInmueble listaInmuebles) {
        this.listaInmuebles.insertarAlFinal(listaInmuebles);
    }

    public void setClientes(ListaEnlazadaServices<Cliente> clientes) {
        this.clientes = clientes;
    }

    /**
     * @param listaInmuebles the listaInmuebles to set
     */
    public void setInmueble(BienInmueble listaInmuebles, Integer posicion) {
        this.listaInmuebles.modificarDatoPosicion(posicion, listaInmuebles);
    }

    public void deleteInmueble(Integer posicion) {
        this.listaInmuebles.eliminarPosicion(posicion);
    }

    /**
     * @return the fasturas
     */
    public ListaEnlazadaServices<Factura> getFasturas() {
        return fasturas;
    }

    public void addFactura(Factura fastura) {
        this.fasturas.insertarAlFinal(fastura);
    }

    /**
     * @return the clientes
     */
    public ListaEnlazadaServices<Cliente> getClientes() {
        return clientes;
    }

    public void addClientes(Cliente cliente) {
        this.clientes.insertarAlFinal(cliente);
    }

}
