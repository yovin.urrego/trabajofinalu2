/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo.binmuebles;

import controlador.utiles.TipoBienInmueble;

/**
 *
 * @author Yovin
 */
public class BienInmueble {

    private String ubicacion;
    private String propietario;
    private String tipoInmueble;
    private Double  costo;

    /**
     * @return the ubicacion
     */
    public String getUbicacion() {
        return ubicacion;
    }

    /**
     * @param ubicacion the ubicacion to set
     */
    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    /**
     * @return the propietario
     */
    public String getPropietario() {
        return propietario;
    }

    /**
     * @param propietario the propietario to set
     */
    public void setPropietario(String propietario) {
        this.propietario = propietario;
    }

    /**
     * @return the tipoInmueble
     */
    public String getTipoInmueble() {
        return tipoInmueble;
    }

    public BienInmueble(String ubicacion, String propietario, TipoBienInmueble tipoInmueble, Double costo) {
        this.ubicacion = ubicacion;
        this.propietario = propietario;
        setTipoInmueble(tipoInmueble);
        this.costo = costo;
    }

    /**
     * @param tipoInmueble the tipoInmueble to set
     */
    public void setTipoInmueble(TipoBienInmueble tipoInmueble) {
        if (tipoInmueble.toString().equalsIgnoreCase(TipoBienInmueble.NATURALEZA.toString())) {
            this.tipoInmueble = TipoBienInmueble.NATURALEZA.toString();
        } else {
            this.tipoInmueble = TipoBienInmueble.INCORPORACION.toString();
        }
    }

    /**
     * @return the costo
     */
    public Double getCosto() {
        return costo;
    }

    /**
     * @param costo the costo to set
     */
    public void setCosto(Double costo) {
        this.costo = costo;
    }
}
