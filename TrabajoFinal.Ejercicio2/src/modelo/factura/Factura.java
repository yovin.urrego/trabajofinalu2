/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo.factura;

import controlador.tda.lista.ListaEnlazadaServices;
import java.sql.Date;
import modelo.binmuebles.BienInmueble;
import modelo.cliente.Cliente;

/**
 *
 * @author Yovin
 */
public class Factura {

    private String fechaEmitida;
    private String cliente;
    private String imnueble;
    private Double totalSinIva;
    private Double total;
    private Double Iva;

    public Factura(String fechaEmitida, String cliente, Double totalSinIva, String inmueble) {
        this.imnueble = inmueble;
        this.fechaEmitida = fechaEmitida;
        this.cliente = cliente;
        this.totalSinIva = totalSinIva;
        this.total = totalSinIva * 0.12 + totalSinIva;
        this.Iva = 0.12;
    }

    /**
     * @return the fechaEmitida
     */
    public String getFechaEmitida() {
        return fechaEmitida;
    }

    /**
     * @param fechaEmitida the fechaEmitida to set
     */
    public void setFechaEmitida(String fechaEmitida) {
        this.fechaEmitida = fechaEmitida;
    }



    /**
     * @return the totalSinIva
     */
    public Double getTotalSinIva() {
        return totalSinIva;
    }

    /**
     * @param totalSinIva the totalSinIva to set
     */
    public void setTotalSinIva(Double totalSinIva) {
        this.totalSinIva = totalSinIva;
    }

    /**
     * @return the total
     */
    public Double getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(Double total) {
        this.total = total;
    }

    /**
     * @return the Iva
     */
    public Double getIva() {
        return Iva;
    }

    /**
     * @param Iva the Iva to set
     */
    public void setIva(Double Iva) {
        this.Iva = Iva;
    }

}
