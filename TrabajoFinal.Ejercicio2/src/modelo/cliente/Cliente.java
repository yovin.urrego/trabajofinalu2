/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo.cliente;

/**
 *
 * @author Yovin
 */
public class Cliente {
    private String nombre;
    private String identificacion;

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    public Cliente(String nombre, String apellido, String identificacion) {
        this.nombre = nombre+" "+ apellido;
        this.identificacion = identificacion;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the identificacion
     */
    public String getIdentificacion() {
        return identificacion;
    }

    /**
     * @param identificacion the identificacion to set
     */
    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }
    
}
